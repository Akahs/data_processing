import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(3.3, 2.6), dpi=300)
analytical = [18.5, 18.8, 19, 18.9, 18.7]
sim = [14.2, 16.2, 17, 16.4, 15.9]
ratio = [0.2, 0.3, 0.4, 0.6, 0.8]
line1, = plt.plot(ratio, analytical, 'o')
line2, = plt.plot(ratio, sim, 'x')
plt.xlabel('Aspect Ratio')
plt.ylabel('Q')
legends = ['analytical', 'simulation']
lines = [line1, line2]
plt.legend(lines, legends, fontsize=10, frameon=False, loc=0)
plt.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))
plt.tight_layout()
plt.show()

