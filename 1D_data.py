# Currently this file is used to draw curves of the ring resonator parameters
import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(3.3, 2.6), dpi=300)
lines = []
folder = 'C:/Users/Kaijun/Google Drive/research/MyPublications/OE17/revision/'
# filename = raw_input('Type in your filename: ')
fileDir1 = folder + 'r45_exp.txt'
# fileDir2 = folder + 'r45_FDTD.txt'
fileDir3 = folder + 'r45TM_COMSOL.txt'
fileDir4 = folder + 'r45TM_COMSOL_undercut.txt'
fileDirs = [fileDir1, fileDir3, fileDir4]
flag = 0
for fileDir in fileDirs:
    if flag < 1:
        data = np.genfromtxt(fileDir, delimiter=',')
    else:
        data = np.genfromtxt(fileDir)
    xdata = data[:, 0]
    ydata = data[:, 1]
    line1, = plt.plot(xdata, ydata)
    lines.append(line1)
    flag += 1
plt.rcParams['font.family'] = 'Arial'
# plt.title('5:5', fontsize=11)
plt.xlim((900, 1200))
plt.ylim((-0.01, 0.31))
plt.xlabel('Wavenumber (cm$^{-1}$)', fontsize=12)
plt.ylabel('Reflection', fontsize=12)
plt.xticks(fontsize=11)
plt.yticks(fontsize=11)
# plt.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))
plt.tight_layout()
legends = ['Measured', 'FEA', 'FEA U.C.']
plt.legend(lines, legends, fontsize=10, loc=1)
# ax = fig.gca()
# ax.grid(ls='--')
plt.show()
bSave = raw_input('Do you want to save the figure? (y/n)\t')
if bSave == 'y':
    fig.savefig(folder+'r45.png')



