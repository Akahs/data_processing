import numpy as np
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(3.3, 2.6), dpi=300)
lines = []
eps_m = 1.61
eps_inf = 10
d_eps = eps_inf - eps_m
omega_t = 884
omega_p = 2551
omega_res = 1011
gamma = 48.5
omega = np.linspace(600, 1400, 200)
V = 0.35 ** 2 * 1e-12  # cm^3
a = 0.5e-4  # cm
c = 3e10
# f1 = (omega ** 2 - omega_t ** 2) / (d_eps * (omega ** 2 - omega_t ** 2) + omega_p ** 2)
# f1_ = (omega_t ** 2 - omega ** 2) / (d_eps * (omega_t ** 2 - omega ** 2) + omega_p ** 2)
f2_num = d_eps * (gamma * omega) ** 2 + (omega_t ** 2 - omega ** 2) * (d_eps * (omega_t ** 2 - omega ** 2) + omega_p ** 2)
f2_denom = (d_eps * (omega_t ** 2 - omega ** 2) + omega_p ** 2) ** 2 + (d_eps * gamma * omega) ** 2
f2_denom2 = (d_eps * (omega_t ** 2 - omega ** 2) + omega_p ** 2) ** 2
f2 = eps_m / V * f2_num / f2_denom
f0 = (2 * np.pi * c * omega) ** 2 / (4 * np.pi * c ** 2 * a)
# f0 = f2_num/f2_denom2
# Expansion regarding omega^2 around omega_t
# f3 = (omega ** 2 - omega_t ** 2) / omega_p ** 2
# Expansion regarding omega around omega_t
# f3 = 2 * omega_t * (omega - omega_t) / omega_p ** 2
# Expansion regarding omega^2 around omega_res
f3 = (omega_res ** 2 - omega_t ** 2) / (d_eps * (omega_res ** 2 - omega_t ** 2) + omega_p ** 2) + \
     (omega ** 2 - omega_res ** 2) * omega_p ** 2 / (d_eps * (omega_res ** 2 - omega_t ** 2) + omega_p ** 2) ** 2
# Expansion regarding omega around omega_res
f4 = (omega_res ** 2 - omega_t ** 2)/(d_eps * (omega_res ** 2 - omega_t ** 2) + omega_p ** 2) + (omega - omega_res) *\
                                                                                                2 * omega_res * omega_p ** 2 / (d_eps * (omega_res ** 2 - omega_t ** 2) + omega_p ** 2) ** 2
# Expansion regarding omega around omega_t
# f4 = 2 * omega_t * (omega - omega_t) / omega_p ** 2 + (omega - omega_t) ** 2 * (omega_p ** 2 - 4 * d_eps * omega_t **
#                                                                                 2)/omega_p ** 4
# line4, = plt.plot(omega, f4, 'r')
# line3, = plt.plot(omega, f3)
line1, = plt.plot(omega, f2)
line2, = plt.plot(omega, f0, '--')
# lines = [line1, line2, line3, line4]
lines = [line1, line2]
plt.xlabel('Wavenumber (1/cm)', fontsize=11)
plt.ylabel('terms', fontsize=11)
plt.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))
plt.title('$\omega^2$ coefficients')
legends = ['Main', 'Correction']
plt.legend(lines, legends, fontsize=10, frameon=False, loc=0)
ax = plt.gca()
gridlines = ax.get_xgridlines() + ax.get_ygridlines()
for line in gridlines:
    line.set_linestyle(':')
plt.grid(True)
plt.tight_layout()
plt.show()
