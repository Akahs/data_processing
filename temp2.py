import numpy as np
c = 3e10  # cm/s
omega_p = 2 * np.pi * c * 2551  # rad/s
omega_res = 2 * np.pi * c * 1011  # rad/s
omega_t = 2 * np.pi * c * 884  # rad/s
gamma = 2 * np.pi * c * 48.5  # rad/s
V = 0.35 ** 2 * 1e-12  # cm^3
eps_m = 1.61
d_eps = 10 - eps_m
a = 0.5e-4  # cm
coeff = (eps_m * omega_p ** 2)/(V * (omega_p ** 2 + d_eps * (omega_res ** 2 - omega_t ** 2)) ** 2)\
        - 1 / (4 * np.pi * a * c ** 2)
print coeff
# omega_wn = np.linspace(600, 1400, 200)
# omega = 2 * np.pi * c * omega_wn
omega = omega_res
im_coeff = omega ** 2 / (6 * np.pi * c ** 3) + (gamma * omega_p ** 2 * eps_m) /\
                                         (V * (omega_p ** 2 + d_eps * (omega ** 2 - omega_t ** 2)) ** 2)
print im_coeff
decay_rate = im_coeff / coeff
print decay_rate / (2 * np.pi * c)
