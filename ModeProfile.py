import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import re

myFig = plt.figure(1, [1.6, 2.4], 300)
folder = '../MyPublications/Q17/'
filename = raw_input('Type in your filename: ')
sComponent = re.search('^E[x,z](?=\d)', filename).group()
# sComponent = raw_input('Which component are we plotting (Ex/Ez)?')
sDoping = re.search('(?<=_)\d\.*\d*e\d+', filename).group()
if sComponent == 'Ex':
    sOmega = re.search('(?<=Ex)\d+(?=_)', filename).group()
else:
    sOmega = re.search('(?<=Ez)\d+(?=_)', filename).group()
# omega = float(sOmega)
# omega = tHz * 1e12 / 3e10
omega = int(sOmega)
fileDir = folder + filename + '.txt'
myFile = open(fileDir, 'r')
header1 = 'x(microns)'
header2 = 'z(microns)'
header3 = 'profile:E'
header1_loc = 0
header2_loc = 0
header3_loc = 0
header1_line = ''
header2_line = ''
for num, line in enumerate(myFile, 1):
    if header1 in line:
        header1_loc = num
        header1_line = line
    elif header2 in line:
        header2_loc = num
        header2_line = line
    elif header3 in line:
        header3_loc = num
sRows1 = re.search('(?<=x\(microns\)\()\d+(?=,1)', header1_line).group()
rows1 = int(sRows1)
sRows2 = re.search('(?<=z\(microns\)\()\d+(?=,1)', header2_line).group()
rows2 = int(sRows2)
# print header1_loc
# print header2_loc
# print header3_loc
xloc = np.genfromtxt(fileDir, skip_header=header1_loc, max_rows=rows1)
zloc = np.genfromtxt(fileDir, skip_header=header2_loc, max_rows=rows2)
field = np.genfromtxt(fileDir, skip_header=header3_loc)
# fMax = np.amax(np.abs(field))
fMax = 0.17
print fMax
field_norm = field/fMax
X, Z = np.meshgrid(xloc, zloc)
# field_type = input('What type of plot this is? 1: value 2: magnitude\t')
cmap = 'bwr'
# if field_type == 1:
#     cmap = 'bwr'
# elif field_type == 2:
#     cmap = 'plasma'
# else:
#     print 'wrong input, using default colormap'
vMin = -1
vMax = 1
# if field_type == 2:
#     vMin = 0
cax = plt.pcolormesh(X, Z, field_norm.T, shading='gouraud', cmap=cmap, vmin=vMin, vmax=vMax)
# plt.title(sComponent + sOmega + ' '+ sDoping, fontsize=11)
# # plt.xlim((800, 1300))
# plt.xlabel('x (um)', fontsize=11)
# plt.ylabel('z (um)', fontsize=11)
plt.rcParams['font.family'] = 'Arial'
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)
plt.axis('off')
# plt.tight_layout()
# Draw cavity boundary
ax = plt.gca()
w = 0.26
h = 1.31
ax.add_patch(Rectangle((-w/2, 0), w, h, fill=None, color='0.2', ls='--'))
cbar = myFig.colorbar(cax, ticks=[-1, 0, 1])
plt.show()
bSave = raw_input('Do you want to save the figure? (y/n)\t')
if bSave == 'y':
    myFig.savefig(folder + filename + '.png')
myFile.close()
# print Z
