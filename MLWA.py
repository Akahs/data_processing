import numpy as np
import matplotlib.pyplot as plt

c = 3e10  # cm/s


class MLWA(object):
    eps_m = 1.61

    def __init__(self, a=0.0, b=0.0, omega_t=0.0, omega_p=0.0, gamma=0.0, eps_inf=0.0):
        self.a = a * 1e-4
        self.V = a * b ** 2 * 1e-12
        self.f_t = omega_t * 2 * np.pi * c
        self.f_p = omega_p * 2 * np.pi * c
        self.gamma = gamma * 2 * np.pi * c
        self.eps_inf = eps_inf
        self.d_eps = eps_inf - self.eps_m
        self.x_t = self.f_t ** 2
        self.x_p = self.f_p ** 2

    def rad_abs(self, omega):
        x = (omega * 2 * np.pi * c) ** 2
        numerator = self.V * x * (
            (self.x_p + self.d_eps * self.x_t - self.d_eps * x) ** 2 + (self.d_eps * self.gamma) ** 2 * x)
        denominator = 6 * np.pi * c ** 3 * self.eps_m * self.gamma * self.x_p
        return numerator / denominator

    def rad_tot(self, omega):
        ratio = self.rad_abs(omega)
        return ratio/(1+ratio)

    # Derivative of numerator
    def dnum_dx(self, x):
        return 2 * self.d_eps * (x - self.x_t) - self.x_p + self.d_eps * self.gamma ** 2

    # Numerator
    def num(self, x):
        # a = self.x_t - x
        a = (self.x_t - x) * (self.x_p + self.d_eps * (self.x_t - x)) + self.d_eps * x * self.gamma ** 2
        # print x
        # print a
        return a

    # Denominator
    def denom(self, x):
        a = (self.x_p + self.d_eps * (self.x_t - x)) ** 2 + x * (self.d_eps * self.gamma) ** 2
        # print x
        # print a
        return a

    # Eta
    def eta(self, omega):
        f = omega * 2 * np.pi * c
        a = self.num(f ** 2)/self.denom(f ** 2)
        # print omega
        return a

    # Derivative of denominator
    def ddenom_dx(self, x):
        return 2 * (x - self.x_t) * self.d_eps ** 2 - 2 * self.d_eps * self.x_p + (self.d_eps * self.gamma) ** 2

    # Derivative of eta
    def deta_dx(self, x):
        return (self.dnum_dx(x) * self.denom(x) - self.ddenom_dx(x) * self.num(x)) / (self.denom(x) ** 2)

    def imCo_rad(self, x):
        return -x / (6 * np.pi * c ** 3)

    def imCo_abs(self, x):
        return -self.eps_m * self.gamma * self.x_p / (self.V * ((self.x_p + self.d_eps * (self.x_t - x)) ** 2 + x *
                                                                (self.d_eps * self.gamma) ** 2))
    # Total decay rate
    def Gamma(self, omega):  # in cm-1
        x = (omega * 2 * np.pi * c) ** 2
        coeff_correction = - 1 / (4 * np.pi * self.a * c ** 2)
        coeff_main = self.eps_m * self.deta_dx(x) / self.V
        omega2Co = coeff_correction + coeff_main
        return (self.imCo_abs(x) + self.imCo_rad(x)) / (2 * np.pi * c * omega2Co)

    # Q-factor
    def Q(self, omega):
        return omega/self.Gamma(omega)

if __name__ == '__main__':
    # omega = 970
    # f = omega * 2 * np.pi * c
    # x = f ** 2
    # mlwa = MLWA(a=0.57, b=0.4, eps_inf=9.97, omega_t=797, omega_p=2274, gamma=47.9)
    # print mlwa.Gamma(omega)
    # print mlwa.Q(omega)
    # print mlwa.rad_tot(omega)
    lOmega = [1071, 1084, 1088, 1087, 1057, 1085, 1094, 1105, 1040, 1081, 1092]
    lB = [0.46, 0.61, 0.81, 0.99, 0.35, 0.71, 0.91, 1.11, 0.31, 0.5, 0.72]
    lQ = []
    for (b, omega) in zip(lB, lOmega):
        mlwa = MLWA(a=1, b=b, omega_t=863, omega_p=2484, gamma=48.3, eps_inf=9.99)
        lQ.append(mlwa.Q(omega))
    plt.rcParams['font.family'] = 'Arial'
    fig = plt.figure(figsize=(3.3, 2.6), dpi=300)
    lQ_exp = [17.89, 18.28, 17.47, 15.9, 18.08, 17.13, 16.2, 16.92, 18.25, 17.53, 16.86]
    # np.savetxt('8.3e18.txt',[lB, lQ, lQ_exp])
    line1, = plt.plot(lB, lQ_exp, 'o')
    line2, = plt.plot(lB, lQ, 'x')
    lines = [line2, line1]
    legends=('MLWA','Measured')
    plt.legend(lines, legends, fontsize=10, loc=0)
    plt.xlabel('Width ($\mu$m)', fontsize=12)
    plt.ylabel('Q', fontsize=12)
    plt.xticks(fontsize=11)
    plt.yticks(fontsize=11)
    # plt.title('8.3e18')
    plt.xlim((0.2, 1.2))
    # plt.ylim((14.5, 21.5))
    plt.tight_layout()
    plt.show()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        fig.savefig('Q_test.eps')


# fig = plt.figure(figsize=(3.3, 2.6), dpi=300)


# c = 3e10  # cm/s
# omega_p = 2 * np.pi * c * 2551  # rad/s
# x_p = omega_p ** 2
# omega_res = 2 * np.pi * c * 1011  # rad/s
# x_res = omega_res ** 2
# omega_t = 2 * np.pi * c * 884  # rad/s
# x_t = omega_t ** 2
# gamma = 2 * np.pi * c * 48.5  # rad/s
# V = 0.35 ** 2 * 1e-12  # cm^3
# eps_m = 1.61
# d_eps = 10 - eps_m
# a = 0.5e-4  # cm

# # eta
# def eta(x):
#     return num(x) / denom(x)
#
# def Gamma_rad(x):  # in cm-1
#     return imCo_rad(x) / (2 * np.pi * c * omega2Co)
#
#
# def Gamma_abs(x):  # in cm-1
#     return imCo_abs(x) / (2 * np.pi * c * omega2Co)


# Ratio of radiative decay vs. absorptive decay
# def rad_abs(x, x_p):
#     numerator = V * x * ((x_p + d_eps * x_t + d_eps * x) ** 2 + (d_eps * gamma) ** 2 * x)
#     denominator = 6 * np.pi * c ** 3 * eps_m * gamma * x_p
#     return numerator / denominator


# print deta_dx(x_res)
# omega_wn = np.linspace(800, 1300, 200)
# omega = 2 * np.pi * c * omega_wn
# x = omega ** 2
# # print omega2Co
#
# line0, = plt.plot(omega_wn, imCo_rad(x) / imCo_abs(x))
# line1, = plt.plot(omega_wn, Gamma_abs(x))
# line2, = plt.plot(omega_wn, Gamma(x))
# line2, = plt.plot(omega_wn, correction(x), '--')
# lines = [line0, line1, line2]
# legends = ['$\Gamma_{rad}$', '$\Gamma_{abs}$', '$\Gamma$']
# plt.legend(lines, legends, fontsize=10, frameon=False, loc=0)
# plt.xlabel('Wavenumber (cm$^{-1}$)', fontsize=11)
# plt.ticklabel_format(style='sci', axis='y', scilimits=(-2, 2))
# plt.ylabel('$\Gamma_{rad}/\Gamma_{abs}$', fontsize=11)
# ax = plt.gca()
# gridlines = ax.get_xgridlines() + ax.get_ygridlines()
# for line in gridlines:
#     line.set_linestyle(':')
# plt.grid(True)
# plt.tight_layout()
# plt.show()
