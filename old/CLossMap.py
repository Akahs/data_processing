# Making the loss map from the simulated bending loss (matrix)
import numpy as np
import matplotlib.pyplot as plt
from CPlots import MapPlot


class LossMap(MapPlot):
    def __init__(self):
        self.folder = '../Si3N4_rib/loss_map/'
        super(LossMap, self).__init__()

    def make_plot(self):
        plt.imshow(self.core, extent=self.extents(self.w) + self.extents(self.t), origin='lower', aspect='auto')
        plt.title('Loss map wl' + self.sWL + ' r' + self.radius.group() + ' tSlab' + self.sTSlab)
        self.setup_plot()
        cbar = plt.colorbar()
        cbar.set_label('Loss (dB/90 deg)')

        # Make boundary
        sBoundary = raw_input("Do you want to draw boundary?(y/n)\t")
        if sBoundary == 'y':
            x = np.array([1.09, 1.09, 1.11, 1.11, 1.13])
            y = np.array([0.255, 0.235, 0.235, 0.205, 0.205])
            plt.plot(x, y, 'w')

        plt.show()

    def save_plot(self):
        self.myFig.savefig(self.fileDir + '.png')

if __name__ == '__main__':
    mode = LossMap()
    mode.make_plot()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        mode.save_plot()
