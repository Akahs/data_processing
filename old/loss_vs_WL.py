import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import PchipInterpolator
sRibSize = raw_input("Type in your rib size (w x t):\t")
sTSlab = raw_input("Type in your tSlab:\t")
# material = "BTO"
folder = "../Si3N4_rib/tSlab0.14/"
sRadius = raw_input("Type in your 3 radii delimited by spaces:\t")
lRadius = sRadius.split()
# file 1
filename = 'loss_vs_WL_'+sRibSize+'_tBTO'+sTSlab+'_r'+lRadius[0]
fileDir = folder+filename+'.txt'
data = np.loadtxt(fileDir, skiprows=1)
r = data[:, 0]
loss = data[:, 1]
x = np.linspace(0.5, 1.7, 100)
# f = interp1d(tBTO,loss,kind='cubic')
f = PchipInterpolator(r, loss)
y = f(x)
line1, line1_ = plt.plot(x, y, r, loss, 'o', label='data')
# file 2
filename = 'loss_vs_WL_'+sRibSize+'_tBTO'+sTSlab+'_r'+lRadius[1]
fileDir = folder+filename+'.txt'
data = np.loadtxt(fileDir, skiprows=1)
r = data[:, 0]
loss = data[:, 1]
f = PchipInterpolator(r, loss)
y = f(x)
line2, line2_ = plt.plot(x, y, r, loss, 'x', label='data')
# file 3
filename = 'loss_vs_WL_'+sRibSize+'_tBTO'+sTSlab+'_r'+lRadius[2]
fileDir = folder+filename+'.txt'
data = np.loadtxt(fileDir, skiprows=1)
r = data[:, 0]
loss = data[:, 1]
f = PchipInterpolator(r, loss)
y = f(x)
line3, line3_ = plt.plot(x, y, r, loss, '^', label='data')
# plot
plt.xlabel('Wavelength (um)')
plt.ylabel('Bending loss (dB/90 deg)')
plt.title('loss vs WL '+ sRibSize +' tSlab'+sTSlab)
plt.legend([line1, line2, line3], lRadius)
plt.savefig(folder+'loss_vs_WL_'+sRibSize+'_tSlab'+sTSlab+'.png')
plt.show()
