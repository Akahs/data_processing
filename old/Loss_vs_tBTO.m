% plot(radius,WGloss,'x--');
% ylabel('WG loss (dB)');
% yyaxis right
x = linspace(0.14,0.22);
y4 = spline(tBTO,loss400,x);
y5 = spline(tBTO,loss500,x);
y6 = spline(tBTO,loss600,x);
plot(x,y4);
hold on;
plot(x,y5);
plot(x,y6);
plot(tBTO,loss400,'x');
plot(tBTO,loss500,'o');
plot(tBTO,loss600,'^');
ylabel('Bending loss (dB/turn)');
xlim([0.13 0.23]);
xlabel('tBTO (um)');
title('BTO 0.44x0.06, WL=1.7um');
ylim([0 10])
legend('400 um','500','600');