import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import re


folder = '../Si3N4_rib/tSlab0.14/'
filename = raw_input("What's your filename?\t")
sWRib = re.search('(?<=_)\d+\.\d+(?=x)', filename).group()
sTRib = re.search('(?<=x)\d+\.\d+(?=_)', filename).group()
sTSlab = re.search('(?<=tSlab)\d+\.\d+(?=_)', filename).group()
sLambda = re.search('(?<=WL)\d+\.\d+(?=_)', filename).group()
sRadius = re.search('(?<=r)\d+', filename).group()
sMethod = re.search('(?<=Sz)\w+(?=_)', filename).group()

sRibSize = str(sWRib) + 'x' + str(sTRib)

iScale = input("Do your want linear (0) or log (1) scale?\t")


fileDir = folder + filename
data = np.genfromtxt(fileDir + '.txt', skip_header=4)
yloc = data[0, 1:]
# print w
# dw = w[1] - w[0]
xloc = data[1:, 0]
# dt = t[0] - t[1]
# print t
mode = abs(data[1:, 1:].T)
modeNorm = mode / mode.max()

X, Y = np.meshgrid(xloc, yloc)
myFig = plt.figure(1, [8, 6], 96)
if iScale:
    plt.pcolormesh(X, Y, modeNorm, shading='gouraud', cmap='jet', norm=colors.LogNorm(1e-6, 1))
else:
    plt.pcolormesh(X, Y, modeNorm, shading='gouraud', cmap='hot')
    #######################
    # Make boundaries
    #######################
    wRib = float(sWRib)
    tRib = float(sTRib)
    tSlab = float(sTSlab)
    wCell = 20
    tCell = 8
    x = np.array([-wCell / 2, -wRib / 2, -wRib / 2, wRib / 2, wRib / 2, wCell / 2])
    y = np.array([tSlab, tSlab, tSlab + tRib, tSlab + tRib, tSlab, tSlab])
    plt.plot(x, y, 'w--', linewidth=1)
    x = np.array([-wCell / 2, wCell / 2])
    y = np.array([0, 0])
    plt.plot(x, y, 'w--', linewidth=1)
    plt.xlim(-2, 2)
    plt.ylim(-1.8, 2)
#######################
# Make labels
#######################
cbar = plt.colorbar()
cbar.set_label('Sz/max(Sz)')
plt.xlabel('x (um)')
plt.ylabel('y (um)')
plt.title('Sz' + sMethod + ' ' + sRibSize + ' tSlab' + sTSlab + ' wl' + sLambda + ' r' + sRadius)
#######################
# Show mesh
#######################
# for i in xloc:
#     x = [i, i]
#     y = [tCell/2, -tCell/2]
#     plt.plot(x,y,'w',linewidth=0.5)
# for i in yloc:
#     x = [-wCell/2, wCell/2]
#     y = [i, i]
#     plt.plot(x,y,'w',linewidth=0.5)
plt.show()
bSave = raw_input('Do you want to save the figure? (y/n)\t')
if bSave == 'y':
    if iScale:
        myFig.savefig(fileDir + '_log.png')
    else:
        myFig.savefig(fileDir + '_linear.png')
