% plot(radius,WGloss,'x--');
% ylabel('WG loss (dB)');
% yyaxis right
x = linspace(0.5,1.7);
% y1 = spline(WL,Bendingloss,x);
% y2 = pchip(WL,loss500,x);
% y3 = pchip(WL,loss600,x);
% semilogy(x,y1);
% hold on;
% plot(x,y2);
% plot(x,y3);
semilogy(WL,Bendingloss,'o--');
% plot(WL,loss500,'x');
% plot(WL,loss600,'^');
ylabel('Bending loss (dB/cm)');
xlim([0.4 1.8]);
xlabel('Wavelength (um)');
title('Si3N4 0.93x0.1, r400um');
% ylim([0 5])
% legend('400 um','500','600');