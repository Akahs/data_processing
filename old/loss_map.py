import numpy as np
import matplotlib.pyplot as plt


def extents(f):
    delta = f[1] - f[0]
    return [f[0] - delta / 2, f[-1] + delta / 2]


folder = '../Si3N4_rib/tSlab0.14/'
filename = raw_input("Type in your filename:\t")
fileDir = folder + filename
sWl = raw_input("What's the wavelength?\t")
sTSlab = raw_input("What's the slab thickness?\t")
sRadius = raw_input("What's the bending radius?\t")
data = np.genfromtxt(fileDir + '.txt')
w = data[0, 1:]
# print w
# dw = w[1] - w[0]
t = data[1:, 0]
# dt = t[0] - t[1]
# print t
mode_num = data[1:, 1:]
plt.imshow(mode_num, extent=extents(w) + extents(t), aspect='auto', origin='lower')
cbar = plt.colorbar()
cbar.set_label('Loss (dB/90 deg)')
plt.xlabel('wRib')
plt.ylabel('tRib')
plt.title('Loss map wl'+sWl+" tSlab"+sTSlab+' r'+sRadius)
# Make boundary
x = np.array([0.595, 0.595, 0.605, 0.605])
y = np.array([0.215,  0.165, 0.165, 0.115])
plt.plot(x, y, 'w')
plt.savefig(fileDir + '.png')
plt.show()
