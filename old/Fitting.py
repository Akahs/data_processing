# Fitting some data with certain math models
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def func(x, a, b, c):
    return a / (x - b) + c


def lorentzian(x, a, b, x0, gamma):
    return a - b / ((x - x0) ** 2 + gamma ** 2)


folder = '../Si3N4_rib/ring_resonator/'
check = raw_input(folder + "\tIs this the correct folder? (y/n)\t")
if check != 'y':
    folder = raw_input("Please type in your folder:\t")

filename = raw_input("Type the filename:\t")
fileDir = folder + filename
data = np.loadtxt(fileDir + '.txt', skiprows=3, delimiter=',')
xdata= data[50:200, 0]
ydata = data[50:200, 1]
iniGuess = [1, 0.1, 1.302, 1e-3]
popt, pcov = curve_fit(lorentzian, xdata, ydata, p0=iniGuess)

wavelength = np.linspace(1.301, 1.303, 100)
t = lorentzian(wavelength, *popt)

print popt
gamma = popt[3]
print 'gamma=' + str(gamma)

plt.plot(wavelength, t)
plt.plot(xdata, ydata, 'x')
# plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel('Wavelength (um)')
plt.ylabel('Transmission')
# plt.title('Si index')
plt.show()
