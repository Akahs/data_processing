import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import PchipInterpolator
import re

folder = '../Si3N4_rib/ring_resonator/'

# fileNum = input("How many lines do you want to plot?\t")
myFig = plt.figure(1, [8, 6], 96)
# symbols = ['x', 'o', '^', 's']
# lines = []
# legends = []
# for i in range(fileNum):
    # file 1
filename = raw_input("What's your filename?\t")
fileDir = folder+filename

# sWRib = re.search('(?<=_)\d+\.\d+(?=x)', filename).group()
# sTRib = re.search('(?<=x)\d+\.\d+(?=_)', filename).group()
# sTSlab = re.search('(?<=tSlab)\d+\.\d+(?=_)', filename).group()
# sLambda = re.search('(?<=wl)\d+\.\d+', filename.lower()).group()
# sRibSize = sWRib+'x'+sTRib

data = np.loadtxt(fileDir+'.txt', skiprows=3, delimiter=',')
wl = data[:, 0]
t = data[:, 1]
tdB = 10 * np.log10(t)
# x = np.linspace(r[0], r[-1], 100)
# f = PchipInterpolator(r, loss)
# y = f(x)
# line, line_ = plt.plot(x, y, r, loss, symbols[i], label='data')
plt.plot(wl, tdB)
# lines.append(line)
# legends.append('wl=' + sLambda + ', tSlab' + sTSlab + ', ' + sRibSize)

plt.xlabel('Wavelength (um)')
plt.ylabel('Power (dB)')
plt.title('Transmission spectrum')
# plt.legend(lines, legends)
plt.show()
# myFig.savefig(fileDir[:-6]+'.png')
