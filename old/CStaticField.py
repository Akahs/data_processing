# Plot the simulated static electric field (cross section)
from CPlots import FieldPlot
import numpy as np
import matplotlib.pyplot as plt


class StaticField(FieldPlot):
    def __init__(self):
        self.folder = None
        super(StaticField, self).__init__()

    def make_plot(self):
        # rRadius = re.search('(?<=r)\d+', self.filename)
        # rElec = re.search('(?<=dElec)\d+', self.filename)
        # sMethod = re.search('(?<=Sz)\w+(?=_)', self.filename).group()
        data = np.genfromtxt(self.fileDir + '.txt', skip_header=3)
        yloc = data[0, 1:]
        xloc = data[1:, 0]
        Ex = data[1:, 1:].T
        ExMax = abs(Ex).max()
        ExNorm = Ex / ExMax
        X, Y = np.meshgrid(xloc, yloc)
        plt.pcolormesh(X, Y, Ex, shading='gouraud', cmap='seismic', vmin=-0.8*ExMax, vmax=0.8*ExMax)

        #######################
        # Set up
        #######################
        self.plot_setup()

        #######################
        # Make labels
        #######################
        cbar = plt.colorbar()
        cbar.set_label('Ex')
        plt.title('Ex ' + self.sRibSize + ' tSlab' + self.sTSlab + ' wl' + self.sLambda + ' straight')

        plt.show()

    def save_plot(self):
        self.myFig.savefig(self.fileDir + '.png')


if __name__ == '__main__':
    mode = StaticField()
    mode.make_plot()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        mode.save_plot()
