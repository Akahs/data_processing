# Super classes for making different plots
from abc import ABCMeta, abstractmethod
import matplotlib.pyplot as plt
import re
import numpy as np


class Plot(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.filename = raw_input("Type the filename:\t")
        self.myFig = plt.figure(1, [8, 6], 96)

    @abstractmethod
    def make_plot(self):
        pass

    @abstractmethod
    def save_plot(self):
        pass


class MapPlot(Plot):

    def __init__(self):
        super(MapPlot, self).__init__()
        # filename = raw_input("Type the filename:\t")
        if self.folder is None:
            self.folder = '../Si3N4_rib/mode_num/'
        check = raw_input(self.folder + "\tIs this the correct folder? (y/n)\t")
        if check != 'y':
            self.folder = raw_input("Please type in your folder:\t")
        self.fileDir = self.folder + self.filename
        self.sTSlab = re.search('(?<=tSlab)\d+\.\d+', self.filename).group()
        self.sWL = re.search('(?<=wl)\d+\.\d+', self.filename.lower()).group()
        self.radius = re.search('(?<=r)\d+', self.filename)
        # self.myFig = plt.figure(1, [8, 6], 96)
        data = np.genfromtxt(self.fileDir + '.txt')
        self.w = data[0, 1:]
        self.t = data[1:, 0]
        self.core = data[1:, 1:]

    @staticmethod
    def setup_plot():
        plt.xlabel('wRib(um)')
        plt.ylabel('tRib(um)')

    @staticmethod
    def extents(f):
        delta = f[1] - f[0]
        return [f[0] - delta / 2, f[-1] + delta / 2]


class FieldPlot(Plot):

    def __init__(self):
        super(FieldPlot, self).__init__()
        if self.folder is None:
            self.folder = '../Si3N4_rib/tSlab0.14/'
        check = raw_input(self.folder + "\tIs this the correct folder? (y/n)\t")
        if check != 'y':
            self.folder = raw_input("Please type in your folder:\t")
        # filename = raw_input("What's your filename?\t")
        self.fileDir = self.folder + self.filename
        # self.myFig = plt.figure(1, [8, 6], 96)
        self.wCell = 16
        self.tCell = 16
        self.sWRib = re.search('(?<=_)\d+\.\d+(?=x)', self.filename).group()
        self.sTRib = re.search('(?<=x)\d+\.\d+(?=_)', self.filename).group()
        self.sTSlab = re.search('(?<=tSlab)\d+\.\d+(?=_)', self.filename).group()
        self.sLambda = re.search('(?<=WL)\d+\.\d+', self.filename).group()
        self.sRibSize = self.sWRib + 'x' + self.sTRib

    def plot_setup(self):
        sBoundary = raw_input("Do you want to draw the boundaries? (y/n)\t")

        if sBoundary == 'y':
            #######################
            # Make boundaries
            #######################
            wRib = float(self.sWRib)
            tRib = float(self.sTRib)
            tSlab = float(self.sTSlab)
            x = np.array([-self.wCell / 2, -wRib / 2, -wRib / 2, wRib / 2, wRib / 2, self.wCell / 2])
            y = np.array([tSlab, tSlab, tSlab + tRib, tSlab + tRib, tSlab, tSlab])
            plt.plot(x, y, 'w--', linewidth=1)
            x = np.array([-self.wCell / 2, self.wCell / 2])
            y = np.array([0, 0])
            plt.plot(x, y, 'w--', linewidth=1)

        #######################
        # Set limits
        #######################
        sDefaultLimit = raw_input(
            "Do you want to use default limits " + str(self.wCell) + 'x' + str(self.tCell) + "? (y/n)\t")
        if sDefaultLimit == 'n':
            self.wCell = input("wCell=\t")
            self.tCell = input("tCell=\t")
        xLower = float(-self.wCell / 2)
        xUpper = float(self.wCell / 2)
        yLower = float(-self.tCell / 2)
        yUpper = float(self.tCell / 2)

        plt.xlim(xLower, xUpper)
        plt.ylim(yLower, yUpper)

        plt.xlabel('x (um)')
        plt.ylabel('y (um)')

        #######################
        # Show mesh
        #######################
        # for i in xloc:
        #     x = [i, i]
        #     y = [self.tCell/2, -self.tCell/2]
        #     plt.plot(x,y,'w',linewidth=0.5)
        # for i in yloc:
        #     x = [-self.wCell/2, self.wCell/2]
        #     y = [i, i]
        #     plt.plot(x,y,'w',linewidth=0.5)
