import numpy as np
import matplotlib.pyplot as plt


def extents(f):
    delta = f[1] - f[0]
    return [f[0] - delta / 2, f[-1] + delta / 2]


filename = raw_input("Type the filename:\t")
folder = '../Si3N4_rib/tSlab0.14/'
fileDir = folder + filename
sWL = raw_input("What's the wavelength?\t")
sTSlab = raw_input("What's the slab thickness?\t")
data = np.genfromtxt(fileDir+'.txt')
w = data[0, 1:]
# print w
# dw = w[1] - w[0]
t = data[1:, 0]
dt = t[0] - t[1]
# print t
mode_num = data[1:, 1:]
plt.imshow(mode_num, extent=extents(w) + extents(t), origin='lower', aspect='auto')
plt.colorbar()
plt.xlabel('wRib(um)')
plt.ylabel('tRib(um)')
plt.title('Mode num wl' + sWL + ' tSlab' + sTSlab)
plt.savefig(fileDir + '.png')
plt.show()
