import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import PchipInterpolator
filename='../Si3N4_rib/TEMPloss_vs_tBTO_0.93x0.1_WL0.5um_r400um.txt'
data = np.loadtxt(filename,skiprows=1)
tBTO = data[:,0]
rev_tBTO = tBTO[::-1]
loss = data[:,1]
rev_loss = loss[::-1]
# print tBTO
x = np.linspace(0.14,0.22,100)
#f = interp1d(tBTO,loss,kind='cubic')
f = PchipInterpolator(rev_tBTO,rev_loss)
y=f(x)
plt.plot(x,y,tBTO, loss, 'o',label='data')
plt.xlabel('tBTO')
plt.ylabel('Bending loss')
plt.title('0.93x0.1 wl0.5um r400um')
plt.show()