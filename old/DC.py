import numpy as np
import matplotlib.pyplot as plt
tSlab = 0.14
tRib = 0.2
wRib = 0.58
ribSize = str(wRib) + "x" + str(tRib)
slabSize = str(tSlab)
filename = '../Si3N4_rib/DC_' + ribSize + '_tSlab' + slabSize
data = np.genfromtxt(filename + '.txt', skip_header=3)
yloc = data[0, 1:]
xloc = data[1:, 0]
Ex = data[1:, 1:].T
ExNorm = Ex / abs(Ex).max()
X, Y = np.meshgrid(xloc, yloc)
plt.figure(1, [8, 6], 96)
plt.pcolormesh(X, Y, ExNorm, shading='gouraud', cmap='jet',vmin=-0.8,vmax=0.8)
cbar = plt.colorbar()
cbar.set_label('Ex')
plt.xlabel('x (um)')
plt.ylabel('y (um)')
plt.title('Ex ' + ribSize + ' tSlab' + slabSize)
plt.xlim(-4, 4)
plt.ylim(-3.8, 4)
#######################
# Make boundaries
#######################
wCell = 20
tCell = 20
x = np.array([-wCell / 2, -wRib / 2, -wRib / 2, wRib / 2, wRib / 2, wCell / 2])
y = np.array([tSlab, tSlab, tSlab + tRib, tSlab + tRib, tSlab, tSlab])
plt.plot(x, y, 'w--', linewidth=1)
x = np.array([-wCell / 2, wCell / 2])
y = np.array([0, 0])
plt.plot(x, y, 'w--', linewidth=1)
plt.show()
