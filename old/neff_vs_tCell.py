import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.interpolate import PchipInterpolator
filename='../BTO_rib/neff_vs_cell_0.35x0.06_tBTO0.18_WL0.5.txt'
data = np.loadtxt(filename,skiprows=1)
tCell = data[:,0]
# rev_tBTO = tBTO[::-1]
neff = data[:,1]
# rev_loss = loss[::-1]
# print tBTO
x = np.linspace(10,160,100)
f = interp1d(tCell,neff,kind='cubic')
# f = PchipInterpolator(tCell,neff)
y=f(x)
plt.plot(x,y,tCell, neff, 'o',label='data')
plt.xlabel('tCell')
plt.ylabel('Neff')
plt.title('0.35x0.06 wl0.5um straight')
plt.show()