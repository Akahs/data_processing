# Plot the ring resonator spectrum
import matplotlib.pyplot as plt
import numpy as np

from CPlots import Plot


class RingSpectrum(Plot):
    def __init__(self):
        super(RingSpectrum, self).__init__()
        folder = '../Si3N4_rib/ring_resonator/'
        check = raw_input(folder + "\tIs this the correct folder? (y/n)\t")
        if check != 'y':
            folder = raw_input("Please type in your folder:\t")
        self.fileDir = folder + self.filename

    def make_plot(self):
        data = np.loadtxt(self.fileDir + '.txt', skiprows=3, delimiter=',')
        wl = data[:, 0]
        t = data[:, 1]
        tdB = 10 * np.log10(t)
        plt.plot(wl, tdB)
        plt.xlabel('Wavelength (um)')
        plt.ylabel('Power (dB)')
        plt.title('Transmission spectrum')
        plt.show()

    def save_plot(self):
        self.myFig.savefig(self.fileDir + '_dB.png')

if __name__ == '__main__':
    spectrum = RingSpectrum()
    spectrum.make_plot()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        spectrum.save_plot()

