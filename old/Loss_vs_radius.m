% plot(radius,WGloss,'x--');
% ylabel('WG loss (dB)');
% yyaxis right
plot(radius,Bendingloss,'o--');
ylabel('Bending loss (dB/turn)');
xlim([300 1100]);
xlabel('Radius (um)');
title('BTO wRib:tRib=0.44:0.06 @1.5um');
ylim([0 1])