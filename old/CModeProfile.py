# Plot the mode profile (Sz) of the waveguide cross section
from CPlots import FieldPlot
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import re


class ModeProfile(FieldPlot):
    def __init__(self):
        self.folder = '../Si3N4_rib/tSlab0.14/'
        super(ModeProfile, self).__init__()
        self.iScale = input("Do your want linear (0) or log (1) scale?\t")

    def make_plot(self):

        rRadius = re.search('(?<=r)\d+', self.fileDir)
        # rElec = re.search('(?<=dElec)\d+', self.filename)
        sMethod = re.search('(?<=Sz)\w+(?=_)', self.fileDir).group()
        data = np.genfromtxt(self.fileDir + '.txt', skip_header=4)
        yloc = data[0, 1:]
        xloc = data[1:, 0]
        mode = abs(data[1:, 1:].T)
        modeNorm = mode / mode.max()
        X, Y = np.meshgrid(xloc, yloc)
        if self.iScale:
            plt.pcolormesh(X, Y, modeNorm, shading='gouraud', cmap='jet', norm=colors.LogNorm(1e-6, 1))
        else:
            plt.pcolormesh(X, Y, modeNorm, shading='gouraud', cmap='hot')

        #######################
        # Set up
        #######################
        self.plot_setup()

        #######################
        # Make labels
        #######################
        cbar = plt.colorbar()
        cbar.set_label('Sz/max(Sz)')
        if rRadius:
            plt.title('Sz' + sMethod + ' ' + self.sRibSize + ' tSlab' + self.sTSlab + ' wl' + self.sLambda + ' r' + rRadius.group())
        else:
            plt.title('Sz' + sMethod + ' ' + self.sRibSize + ' tSlab' + self.sTSlab + ' wl' + self.sLambda + ' straight')

        plt.show()

    def save_plot(self):
        if self.iScale:
            self.myFig.savefig(self.fileDir + '_log.png')
        else:
            self.myFig.savefig(self.fileDir + '_linear.png')


if __name__ == '__main__':
    mode = ModeProfile()
    mode.make_plot()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        mode.save_plot()
