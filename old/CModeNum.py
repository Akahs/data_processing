# making the total mode number map from the simulation result (matrix)
import numpy as np
import matplotlib.pyplot as plt
from CPlots import MapPlot


class ModeNum(MapPlot):
    def __init__(self):
        self.folder = '../Si3N4_rib/mode_num/'
        super(ModeNum, self).__init__()

    def make_plot(self):
        plt.imshow(self.core, extent=self.extents(self.w) + self.extents(self.t), origin='lower', aspect='auto')
        plt.title('Mode num wl' + self.sWL + ' tSlab' + self.sTSlab)
        self.setup_plot()
        cbar = plt.colorbar()
        cbar.set_label('Number of modes')
        plt.show()

    def save_plot(self):
        self.myFig.savefig(self.fileDir + '.png')

if __name__ == '__main__':
    mode = ModeNum()
    mode.make_plot()
    bSave = raw_input('Do you want to save the figure? (y/n)\t')
    if bSave == 'y':
        mode.save_plot()
