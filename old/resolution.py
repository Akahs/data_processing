# This file is used to calculate the baseline resolution of the spectrometer
import numpy as np
import matplotlib.pyplot as plt

# fileDir = "../Si3N4_rib/tSlab0.14/tuning_tSlab0.14_FD.txt"
# data = np.loadtxt(fileDir)
# wavelength = data[:, 0]
# dN_dV = data[:, 1]
# plt.plot(wavelength, dN_dV, 'x')

# L = np.arange(20, 90, 20)
# # dN_dV = np.array([1.03e-4, 6.64e-5, 5.14e-5, 3.94e-5])
voltage = np.arange(20, 55, 10)
wavelength = np.linspace(0.5, 1.7, 100)
dN_dV = 6.36e-5/(wavelength+0.172)+7.15e-6
# L = 18.256  # mm
L = 50  # mm
OPD = np.zeros((4, 100))
for i in range(4):
    OPD[i, :] = voltage[i] * dN_dV * L / 10
resolution = 1/OPD
lines = []
for i in range(4):
    line, = plt.plot(wavelength, resolution[i, :])
    lines.append(line)
plt.xlabel('Wavelength (um)')
plt.ylabel('Baseline Resolution (cm^-1)')
plt.legend(lines, ['20 V', '30', '40', '50'])
plt.grid(linestyle=':')
plt.title('0.57x0.16 tSlab0.14 Pockles=150 pm/V arm=50mm')
# plt.ylim(0, 400)
plt.show()
