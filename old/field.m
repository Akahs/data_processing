length = 257;
x = output(2:length,1);
y = output(1,2:length);
mode = output(2:length,2:length)';
% y_corrected = fliplr(y);
% mode_corrected = flipud(mode);
mode_norm = mode./max(mode(:));
pcolor(x,y,real(log10(mode_norm)));
shading interp
c=colorbar;
ylabel(c,'Ex (V/m)');
set(gca,'YDir','normal')
colormap(hot)
caxis([-6 0]);
% WG structure
wCell = 20;
wRib = 0.58;
tRib = 0.2;
tBTO = 0.14;
X1 = [-wCell/2 wCell/2];
Y1 = [0 0];
X2 = [-wCell/2 -wRib/2 -wRib/2  wRib/2 wRib/2 wCell/2];
Y2 = [tBTO tBTO tBTO+tRib tBTO+tRib tBTO tBTO];
hold on;
line(X1,Y1,'Color','w','LineStyle','--');
line(X2,Y2,'Color','w','LineStyle','--');
hold off;
xlabel('x (um)');
ylabel('y (um)');
xlim([-3 3]);
ylim([-2.7 3]);
title('0.96x0.14 r400 WL1.7');

%% Grid
figure(1);
hold on;
for k=1:length-1
    line([x(k) x(k)],[10 -10]);
    line([-10 10],[y(k) y(k)]);
end
X1 = [-10 10];
Y1 = [0 0];
X2 = [-10 -0.225 -0.225 0.225 0.225 10];
Y2 = [0.17 0.17 0.22 0.22 0.17 0.17];
hold on;
line(X1,Y1,'Color','k');
line(X2,Y2,'Color','k');
hold off;
xlabel('x (um)');
ylabel('y (um)');