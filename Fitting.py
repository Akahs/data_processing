# Fitting some data with certain math models
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def func(x, a, b, c):
    return a / (x - b) + c


def lorentzian(x, eps_inf, omega_t, omega_p, gamma):
    return eps_inf + omega_p ** 2 * (omega_t ** 2 - x ** 2) / ((x ** 2 - omega_t ** 2) ** 2 + (gamma * x) ** 2)


def eps_im(x, omega_t, omega_p, gamma):
    return omega_p ** 2 * gamma * x / ((x ** 2 - omega_t ** 2) ** 2 + (gamma * x) ** 2)


folder = './'
check = raw_input(folder + "\tIs this the correct folder? (y/n)\t")
if check != 'y':
    folder = raw_input("Please type in your folder:\t")

filename = raw_input("Type the filename:\t")
fileDir = folder + filename
data = np.loadtxt(fileDir + '.txt')
xdata = data[:, 0]
ydata = data[:, 1]
iniGuess = [10, 810, 2300, 48]
# iniGuess = [900, 3000, 50]
# iniGuess = [870, 1e6, 10]
popt, pcov = curve_fit(lorentzian, xdata, ydata, p0=iniGuess)
# popt, pcov = curve_fit(eps_im, xdata, ydata, p0=iniGuess)

# wavelength = np.linspace(1.301, 1.303, 100)
omega = np.linspace(600, 1400, 100)
t = lorentzian(omega, *popt)
# t = eps_im(omega, *popt)

print popt
# gamma = popt[3]
# print 'gamma=' + str(gamma)

plt.plot(omega, t)
plt.plot(xdata, ydata, 'x')
# plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.xlabel('Frequency (1/cm)')
plt.ylabel('Permittivity')
# plt.title('Si index')
plt.show()
